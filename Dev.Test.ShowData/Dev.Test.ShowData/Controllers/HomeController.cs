﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dev.Test.ShowData.Models;
using System.Configuration;
using Dev.Test.ShowData.Helper;
namespace Dev.Test.ShowData.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// This is a default controller
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            ViewBag.Title = "Pets Details";

            //Getting the service end point from config file
            string serviceURL = ConfigurationManager.AppSettings["ServiceURL"];
            DataHelper objDataHelper = new DataHelper();

            //passing the end point and calling helper method to get the pets details 
            List<AllDetails> lstAllDetails = objDataHelper.GetPetsDetails(serviceURL);

            return View(lstAllDetails);
        }

        /// <summary>
        /// This controller will return the final JSON object with gender and cat details to client to rander
        /// </summary>
        /// <returns></returns>
        public ActionResult GetPetsDetails() {
            //Getting the service end point from config file
            string serviceURL = ConfigurationManager.AppSettings["ServiceURL"];             
            DataHelper objDataHelper = new DataHelper();

            //passing the end point and calling helper method to get the pets details 
            List<AllDetails> lstAllDetails = objDataHelper.GetPetsDetails(serviceURL);            
            return Json(lstAllDetails, JsonRequestBehavior.AllowGet);
        }
    }
}
