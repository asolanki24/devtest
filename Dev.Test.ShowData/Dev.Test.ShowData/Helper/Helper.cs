﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dev.Test.ShowData.Models;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;

namespace Dev.Test.ShowData.Helper
{
    public class DataHelper
    {
        /// <summary>
        /// This method will get the data from sevice and parse the results to one single data object
        /// </summary>
        /// <param name="ServiceURL"></param>
        /// <returns></returns>
        public List<AllDetails> GetPetsDetails(string ServiceURL)
        {
            List<AllDetails> lstAllDetails = new List<AllDetails>();
            using (var client = new System.Net.Http.HttpClient())
            {
                client.BaseAddress = new Uri(ServiceURL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync(ServiceURL).Result;
                if (response.IsSuccessStatusCode)
                {
                    //declaring list objects to filter the data on basis or gender 
                    List<OwnerDetails> ownerDetails = (new JavaScriptSerializer()).Deserialize<List<OwnerDetails>>(response.Content.ReadAsStringAsync().Result);
                    List<PetDetails> maleCatsDetails = ownerDetails.Where(x => x.gender == "Male" && x.pets != null).SelectMany(x => x.pets).ToList<PetDetails>();
                    List<PetDetails> femaleCatsDetails = ownerDetails.Where(x => x.gender == "Female" && x.pets!=null ).SelectMany(x => x.pets).ToList<PetDetails>();

                    //getting all cats names for males
                    List<string> maleOwnerCats  = maleCatsDetails.Where(x => x.type == "Cat").Select(x => x.name).ToList<string>();
                    AllDetails allDetails = new AllDetails();
                    allDetails.Gender = "Male";
                    allDetails.Name = maleOwnerCats;
                    lstAllDetails.Add(allDetails);

                    //getting all cates names for females
                    List<string> femaleOwnerCats = femaleCatsDetails.Where(x => x.type == "Cat").Select(x => x.name).ToList<string>();
                    allDetails = new AllDetails();
                    allDetails.Gender = "Female";
                    allDetails.Name = femaleOwnerCats;
                    lstAllDetails.Add(allDetails);
                }
            }
            return lstAllDetails;
        }
    }
}