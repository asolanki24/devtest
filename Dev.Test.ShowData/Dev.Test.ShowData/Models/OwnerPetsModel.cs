﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dev.Test.ShowData.Models
{
    public class OwnerPetsModel
    {

    }

    /// <summary>
    /// class for holding pet owner details
    /// </summary>
    public class OwnerDetails
    {
        public string name { get; set; }
        public int age { get; set; }
        public string gender { get; set; }

        public List<PetDetails> pets { get; set; }
    }

    /// <summary>
    /// class for holding pets details
    /// </summary>
    public class PetDetails
    {
        public string name { get; set; }
        public string type { get; set; }
    }

    /// <summary>
    /// class for holding combined data for males and females 
    /// </summary>
    public class AllDetails
    {
        public string Gender { get; set; }
        public List<string> Name = new List<string>();
    }
}